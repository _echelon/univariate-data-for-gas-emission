import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import datetime
import tensorflow as tf
from sklearn.metrics import mean_squared_error, r2_score

# Load gas data
data = pd.read_csv("feed(2).csv")
print(data.head())
data['created_at'] = pd.to_datetime(data['created_at'])
data = data.sort_values(by='created_at') 

# Check for null values
print(f"Number of empty cells: {data.isnull().any(axis=1).mean()}")
print(data.describe())
print(data.info())

#rename columns for gas analysis
data.rename(columns={'field1': 'gas1', 'field2': 'gas2', 'field3': 'gas3', 'field4': 'gas4', 'field5': 'report'}, inplace=True)
print(data.head())

#future prediction on gas1 - univariate data
df = data['gas1']

# Train and test split
msk = (df.index < df.index[-1] - 20)
df_train = df[msk].copy()
df_test = df[~msk].copy()

df_train.plot()
df_test.plot()

# Normalizing data
uni_data = df.values.astype(float)
df_train_len = len(df_train)
uni_train_mean = uni_data[:df_train_len].mean()
uni_train_std = uni_data[:df_train_len].std()
uni_data = (uni_data - uni_train_mean) / uni_train_std
print(uni_data)

# Model data creation functions
def univariate_data(dataset, start_value, end_value, history_size, target_size):
    data = []
    labels = []

    start_value = start_value + history_size
    if end_value is None:
        end_value = len(dataset) - target_size

    for i in range(start_value, end_value):
        indices = range(i - history_size, i)
        data.append(np.reshape(dataset[indices], (history_size, 1)))
        labels.append(dataset[i + target_size])
    return np.array(data), np.array(labels)

# Defining time steps
def create_time_steps(length):
    return list(range(-length, 0))

def show_plot(plot_data, delta, title):
    labels = ['History', 'Future', 'Model Prediction']
    markers = ['.-', 'rx', 'go']
    time_steps = create_time_steps(plot_data[0].shape[0])

    if delta:
        future = delta
    else:
        future = 0

    plt.title(title)
    for i, x in enumerate(plot_data):
        if i:
            plt.plot(future, x, markers[i], markersize=10, label=labels[i])
        else:
            plt.plot(time_steps, x.flatten(), markers[i], label=labels[i])
    plt.legend()
    plt.xlim(time_steps[0], (future + 5) * 2)
    plt.xlabel('Time Step')
    return plt

# Creating the data for univariate model
univariate_past = 50
univariate_future = 5

x_train_uni, y_train_uni = univariate_data(uni_data, 0, None, univariate_past, univariate_future)
x_val_uni, y_val_uni = univariate_data(uni_data, 0, None, univariate_past, univariate_future)

print(x_train_uni[0])
print(y_train_uni[0])

# Check the shape of input data
print("Shape of x_train_uni:", x_train_uni.shape)
print("Shape of x_val_uni:", x_val_uni.shape)

# Building the RNN model
batch_size = 82
buffer_size = 200
epochs = 20

train_univariate = tf.data.Dataset.from_tensor_slices((x_train_uni, y_train_uni))
train_univariate = train_univariate.cache().shuffle(buffer_size).batch(batch_size).repeat()

val_univariate = tf.data.Dataset.from_tensor_slices((x_val_uni, y_val_uni))
val_univariate = val_univariate.batch(batch_size).repeat()

# LSTM
tf.keras.backend.clear_session()
simple_lstm = tf.keras.models.Sequential([
    tf.keras.layers.LSTM(12, input_shape=x_train_uni.shape[-2:]),
    tf.keras.layers.Dense(1)
])

simple_lstm.compile(optimizer='adam', loss='mse')
simple_lstm.summary()

#Fit LSTM model
history = simple_lstm.fit(train_univariate, epochs=epochs, steps_per_epoch=200, validation_data=val_univariate, validation_steps=50)

# Predict model
for x, y in val_univariate.take(2):
    predictions = simple_lstm.predict(x)
    predicted_values = predictions.flatten()[:univariate_future]
    #print("Actual Values:", y[0].numpy())
    #print("Predicted Values:", predicted_values)

# Convert the predicted values and actual values back to the original scale
predicted_values_original_scale = (predicted_values * uni_train_std) + uni_train_mean
actual_values_original_scale = (y[0].numpy() * uni_train_std) + uni_train_mean

print("Actual Values:", actual_values_original_scale)
print("Predicted Values:", predicted_values_original_scale)

plot = show_plot([x[0].numpy(), y[0].numpy(), simple_lstm.predict(x)[0]], 0, "SimpleLSTM")
plot.show()